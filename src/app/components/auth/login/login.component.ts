import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ReturnMessage } from 'src/app/lib/data/models';
import { UserLoginModel } from 'src/app/lib/data/models/users/user.model';
import { AuthService } from 'src/app/lib/data/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  public registerForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private activedRoute: ActivatedRoute,
    private router: Router,
    ) {
    this.createLoginForm();
    this.createRegisterForm();
  }

  get f() {
    return this.loginForm.controls;
  }
  owlcarousel = [
    {
      title: 'Welcome to Multi Cloud',

    }
  ]
  owlcarouselOptions = {
    loop: true,
    items: 1,
    dots: true
  };

  createLoginForm() {
    this.loginForm = this.formBuilder.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
    })
  }
  createRegisterForm() {
    this.registerForm = this.formBuilder.group({
      userName: [''],
      password: [''],
      confirmPassword: [''],
    })
  }


  ngOnInit() {
  }

  onLogin() {
    if (this.loginForm.invalid) {
      return;
    }
    var data: UserLoginModel = this.loginForm.value;
    this.authService
      .login(data)
      .then((res: ReturnMessage<UserLoginModel>) => {
        localStorage.setItem('token', res.data.username);
        localStorage.setItem('idUser', res.data.id);
        this.backUrl();
      })
    
      .catch((er) => {
        window.alert(er.error.message);
      });
  }

  backUrl() {
    var returnUrl = this.activedRoute.snapshot.queryParams['returnUrl'] || '/';
    this.router.navigateByUrl(returnUrl);
  }

}
