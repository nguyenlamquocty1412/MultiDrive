import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PageModel, ReturnMessage } from 'src/app/lib/data/models';
import { Files } from 'src/app/lib/data/models/file/file.model';
import { AccountsService } from 'src/app/lib/data/services';
import { ETypeGridLayout } from 'src/app/shared/data/common/common.model';

@Component({
  selector: 'app-file-details',
  templateUrl: './file-details.component.html',
  styleUrls: ['./file-details.component.scss'],
  providers: [AccountsService]
})
export class FileDetailsComponent implements OnInit {
  public grid: string = ETypeGridLayout.NORMAL;
  public files: Files[];
  public data: PageModel<Files>;
  public id: any;
  public check = false;
  constructor
  (
    private accountService: AccountsService,
    private activatedRoute: ActivatedRoute,
  )
  { }

  ngOnInit() {
    this.fetch();
  }

  fetch(){
    this.activatedRoute.queryParams.subscribe((param) => {
      this.accountService
        .getFileInFolder(param.id, param.email)
        .then((res:ReturnMessage<PageModel<Files>>) => {
          if (!res.hasError) {
            this.files = res.data.results;
            this.data = res.data;
          }
        })
    })
  }
}
