import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReturnMessage } from 'src/app/lib/data/models';
import { DriveModel } from 'src/app/lib/data/models/drive/drive.model';
import { AccountsService } from 'src/app/lib/data/services';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss'],
  providers: [AccountsService]
})
export class CreateAccountComponent implements OnInit {

  public accountForm: FormGroup;
  public drive: DriveModel;
  constructor
  (
     private formBuilder: FormBuilder,
     private accountService: AccountsService
  )
  {
    this.createAccountForm();
  }

  ngOnInit() {
    
  }
  get accountFormControls() {
    return this.accountForm.controls;
  }

  createAccountForm() {
    this.accountForm = this.formBuilder.group({
      username: [null, [Validators.required]]
    })
  }

  create(){
    
    this.drive = {
      email: this.accountForm.value.username,
      userId: localStorage.getItem("idUser")
    };

    this.accountService
      .create(this.drive)
      .then((res: ReturnMessage<DriveModel>) => {
        if (!res.hasError) {
          this.drive = res.data;
          window.alert(res.message);
        }
      })
      .catch((er) => {
        console.log(er.error.message);
      })
  }
}
