import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllAccountsComponent } from './all-accounts/all-accounts.component';
import { CreateAccountComponent } from './create-account/create-account/create-account.component';
import { FileDetailsComponent } from './file-details/file-details/file-details.component';
import { SingleAccountsComponent } from './single-accounts/single-accounts.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'multi-drive',
        component: AllAccountsComponent,
        data: {
          title: "Multi Drive",
          breadcrumb: "Multi Drive"
        }
      },
      {
        path: 'single-drive',
        component: SingleAccountsComponent,
        data: {
          title: "Single Drive",
          breadcrumb: "Single Drive"
        }
      },
      {
        path: 'file-details',
        component: FileDetailsComponent,
        data: {
          title: "File",
          breadcrumb: "File"
        }
      },
      {
        path: 'create-account',
        component: CreateAccountComponent,
        data: {
          title: "Create",
          breadcrumb: "Create"
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AccountsRoutingModule { }
