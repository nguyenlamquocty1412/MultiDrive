import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { AccountsRoutingModule } from './account-routing.module';
import { SingleAccountsComponent } from './single-accounts/single-accounts.component';
import { AllAccountsComponent } from './all-accounts/all-accounts.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FileDetailsComponent } from './file-details/file-details/file-details.component';
import { CreateAccountComponent } from './create-account/create-account/create-account.component';

@NgModule({
  declarations: 
  [
    AllAccountsComponent,
    SingleAccountsComponent,
    FileDetailsComponent,
    CreateAccountComponent
  ],
  imports: [
    CommonModule,
    AccountsRoutingModule,
    ReactiveFormsModule,
    NgbModule,
    Ng2SmartTableModule,
    SharedModule
  ]
})
export class AccountModule { }
