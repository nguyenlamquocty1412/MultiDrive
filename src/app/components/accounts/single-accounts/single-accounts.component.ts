import { HttpClient, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PageModel, ReturnMessage } from 'src/app/lib/data/models';
import { Files } from 'src/app/lib/data/models/file/file.model';
import { FolderModel } from 'src/app/lib/data/models/file/folder.model';
import { AccountsService } from 'src/app/lib/data/services';
import { HttpClientService } from 'src/app/lib/http/http-client';
import { ETypeGridLayout, ETypePositionCart } from 'src/app/shared/data/common/common.model';

@Component({
  selector: 'app-single-accounts',
  templateUrl: './single-accounts.component.html',
  styleUrls: ['./single-accounts.component.scss'],
  providers: [AccountsService]
})
export class SingleAccountsComponent implements OnInit {
  public grid: string = ETypeGridLayout.NORMAL;
  public emailForm: FormGroup;
  public folderForm: FormGroup;
  public fileForm: FormGroup;
  public email: string;
  public files: Files[];
  public mess: string;
  public submitted: boolean = false;
  public data: PageModel<Files>;
  public id: any;
  public folder: FolderModel;
  typePositionCart: string = ETypePositionCart.BOX_2;
  public check = false;

  constructor(private accountService: AccountsService, private formBuilder: FormBuilder, private http: HttpClientService) 
  { 
    this.createEmailForm();
    this.createFolderForm();
    this.uploadFileForm();
  }

  ngOnInit() {
    
  }

  deleteItem(id: string, email: string){
    if(window.confirm("Are you sure to Delete ?")){
      this.accountService.delete(id, email).then((res: ReturnMessage<string>) => {
        if (!res.hasError) {
          window.alert(res.message);
          this.fetch();
        }
      })
    }
  }

  fetch(){
    if(this.emailForm.invalid){
      this.submitted = true;
      this.files = null;
    }
    this.email = this.emailForm.value.username;
    this.accountService.getSingleDrive(this.email).then((res: ReturnMessage<PageModel<Files>>) => {
      if (!res.hasError) {
        this.files = res.data.results;
        this.data = res.data;
        this.check = true;
        this.submitted = false;
      }
    })
    .catch((er) => {
      this.submitted = true;
      this.mess = er.error.message;
      this.files = null;
    })
  }

  get emailFormControls() {
    return this.emailForm.controls;
  }

  get folderFormControls() {
    return this.folderForm.controls;
  }

  get fileFormControls() {
    return this.fileForm.controls;
  }

  createEmailForm() {
    this.emailForm = this.formBuilder.group({
      username: [null, [Validators.required, Validators.email]]
    })
  }

  createFolderForm() {
    this.folderForm = this.formBuilder.group({
      nameFolder: [null, [Validators.required]]
    })
  }
  createFolder(){
    this.folder = {
      name: this.folderForm.value.nameFolder,
      email: this.emailForm.value.username
    }
    this.accountService.createFolder(this.folder).then((res: ReturnMessage<string>) => {
      if (!res.hasError) {
        window.alert(res.message);
        this.fetch();
      }
    })
    .catch((er) => {
      window.alert(er.error.message);
    })
  }

  uploadFileForm() {
    this.fileForm = this.formBuilder.group({
      nameFile: [null, [Validators.required]]
    })
  }

  onFileSelected(event){
    const formData = new FormData();
    const fileToUpload = event.target.files[0] as File;
    formData.append('file', fileToUpload, fileToUpload.name)
    formData.append('email', this.emailForm.value.username)
    this.accountService.uploadFile(formData).then((res: ReturnMessage<string>) => {
      if (!res.hasError) {
        window.alert(res.message);
        this.fetch();
      }
    })
    .catch((er) => {
      window.alert(er.error.message);
    })
  }
}
