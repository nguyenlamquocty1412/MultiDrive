import { Component, OnInit } from '@angular/core';
import { PageModel, ReturnMessage } from 'src/app/lib/data/models';
import { Files } from 'src/app/lib/data/models/file/file.model';
import { AccountsService } from 'src/app/lib/data/services';
import { ETypeGridLayout, ETypePositionCart } from 'src/app/shared/data/common/common.model';

@Component({
  selector: 'app-all-accounts',
  templateUrl: './all-accounts.component.html',
  styleUrls: ['./all-accounts.component.scss'],
  providers: [AccountsService]
})
export class AllAccountsComponent implements OnInit {
  typePositionCart: string = ETypePositionCart.BOX_2;
  public grid: string = ETypeGridLayout.NORMAL;
  public files: Files[];
  public data: PageModel<Files>;
  public id: any;
  public check = false;
  constructor(private accountService: AccountsService ) { }

  ngOnInit() {
    this.fetch();
  }


  fetch(){
    this.accountService.get(null).then((res: ReturnMessage<PageModel<Files>>) => {
      if (!res.hasError) {
        this.files = res.data.results;
        this.data = res.data;
      }
    })
  }
  deleteItem(id: string, email: string){
    if(window.confirm("Are you sure to Delete this file?")){
      this.accountService.delete(id, email).then((res: ReturnMessage<string>) => {
        if (!res.hasError) {
          window.alert(res.message);
          this.fetch();
        }
      })
    }
  }
}
