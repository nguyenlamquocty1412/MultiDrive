export interface Owner{
    kind: string,
    displayName: string,
    photoLink: string,
    me: boolean,
    permissionId: string,
    emailAddress: string
}