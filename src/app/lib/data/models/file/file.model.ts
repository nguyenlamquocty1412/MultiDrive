export interface  Files{
    id:string;
    name:string;
    mimetype:string;
    email: string;
    filepath: string;
    parents: [];
    owners: [
        kind: string,
        displayName: string,
        photoLink: string,
        me: boolean,
        permissionId: string,
        emailAddress: string
    ];
}