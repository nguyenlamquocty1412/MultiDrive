export interface UserLoginModel {
    id: string;
    username: string;
    password: string;
  }
  