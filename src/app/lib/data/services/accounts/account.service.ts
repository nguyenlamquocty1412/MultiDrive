import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppConfig } from "src/app/lib/environments/config/appConfig";
import { HttpClientService } from "src/app/lib/http/http-client";
import { SupplierModel } from "../../models";
import { DriveModel } from '../../models/drive/drive.model';
import { Files } from '../../models/file/file.model';
import { FolderModel } from '../../models/file/folder.model';

@Injectable({
  providedIn: 'root',
})
export class AccountsService  {

    private url = '/api/drive';
    private url1 = '/api/getFileInFolder';
    constructor(private httpClient: HttpClientService) { }

    get(request: any) {
      return this.httpClient.getObservable(this.url, request).toPromise();
    }

    create(model: DriveModel) {
      return this.httpClient.postObservable(this.url, model).toPromise();
    }

    update(model: SupplierModel) {
      return this.httpClient.putObservable(this.url, model).toPromise();
    }

    delete(id: string, email: string) {
      const url = `${this.url}/?Id=${id}&email=${email}`;
      return this.httpClient.deleteObservable(url).toPromise();
    }
    getFileInFolder(id: string, email: string) {
      const url = `${'/api/file/getFileInFolder'}?Id=${id}&email=${email}`;
      return this.httpClient.getObservable(url).toPromise();
    }

    getSingleDrive(request: string){
      const url = `${'/api/drive/getSingleDrive'}?email=${request}`;
      return this.httpClient.getObservable(url).toPromise();
    }

    createFolder(request: FolderModel){
      const url = `${'/api/drive/createFolder'}`;
      return this.httpClient.postObservable(url, request).toPromise();
    }
    
    uploadFile(request: any){
      const url = `${'/api/file/uploadFile'}`;
      return this.httpClient.postObservable(url, request).toPromise();
    }
  }
