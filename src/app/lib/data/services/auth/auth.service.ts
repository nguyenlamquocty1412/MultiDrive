import { Injectable } from '@angular/core';
import { HttpClientService } from 'src/app/lib/http/http-client';
import { UserLoginModel } from '../../models/users/user.model';

@Injectable({
    providedIn: 'root',
  })
  export class AuthService {
    constructor(private http: HttpClientService) {}
  
    private url = '/api/auth/login';
  
    ngOnInit(): void {}
  
    login(body: UserLoginModel) {
      return this.http.postObservable(this.url, body).toPromise();
    }
}