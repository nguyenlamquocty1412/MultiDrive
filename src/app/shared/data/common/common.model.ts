import { bind } from "mousetrap";

export class CommonShare {}

export enum ETypePositionInformation {
  BOX_1_4 = "product-detail", //left
  BOX_2 = "product-detail text-center",
  BOX_3_5 = "product-info",
}

export enum ETypePositionCart {
  BOX_1_3 = "cart-info cart-wrap", //right
  BOX_2 = "cart-box",
  BOX_4 = "cart-info",
  BOX_5 = "cart-detail",
}

export enum ETypeSizeImage {
  SMALL = "height: 160px; width: 112.5px;",
  NORMAL = "height: 260px; width: 180px;",
  MEDIUM = "height: 360px; width: 255px;",
  LARGE = "height: 560px; width: 400px;",
}

export enum ETypeGridLayout {
  VERYSMALL = "col-lg-12",
  SMALL = "col-lg-2",
  NORMAL = "col-lg-3",
  MEDIUM = "col-lg-4",
  LARGE = "col-lg-6",
}

export enum ETypeLayoutView {
  GRIDVIEW = "grid-view",
  LISTVIEW = "list-view",
}

export enum TypeDisplayImage {
  LOGO_IMAGE
}

export const TemplateType = {
  Display: {
    [TypeDisplayImage.LOGO_IMAGE]: "logo-image",
  },
};
