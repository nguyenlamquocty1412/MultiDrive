import { Component, Input, OnInit } from '@angular/core';
import { PageModel, ReturnMessage } from 'src/app/lib/data/models';
import { Files } from 'src/app/lib/data/models/file/file.model';
import { Owner } from 'src/app/lib/data/models/file/owner.model';
import { AccountsService } from 'src/app/lib/data/services';
import { ETypeGridLayout, ETypePositionCart } from 'src/app/shared/data/common/common.model';
import { fileURLToPath } from 'url';

@Component({
  selector: 'app-file-box',
  templateUrl: './file-box.component.html',
  styleUrls: ['./file-box.component.scss'],
  providers: [AccountsService]
})
export class FileBoxComponent implements OnInit {
  @Input() typePositionCart: string = ETypePositionCart.BOX_2;
  @Input() file: Files;
  @Input() typeGridLayout: string = ETypeGridLayout.NORMAL;
  @Input() mimeType: string;
  @Input() email: any;
  public imageSrc: string;
  public folder: boolean = false;
  constructor(private accountService: AccountsService) { }
  ngOnInit() {
    switch(this.mimeType){
      case "application/vnd.google-apps.folder":{
        this.imageSrc = "assets/images/folder.jpg"
        this.folder = true;
        break;
      }
      case "application/octet-stream":{
        this.imageSrc = "assets/images/file.jpg"
        break;
      }
      case "image/jpeg":{
        this.imageSrc = "assets/images/Image.jpg"
        break;
      }
      case "image/png":{
        this.imageSrc = "assets/images/Image.jpg"
        break;
      }
      case "application/vnd.google-apps.spreadsheet":{
        this.imageSrc = "assets/images/spreadsheet.jpg"
        break;
      }
      case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":{
        this.imageSrc = "assets/images/spreadsheet.jpg"
        break;
      }
      case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":{
        this.imageSrc = "assets/images/officedocument.jpg"
        break;
      }
      case "application/vnd.google-apps.document":{
        this.imageSrc = "assets/images/word.jpg"
        break;
      }
      case "application/vnd.google-apps.presentation":{
        this.imageSrc = "assets/images/presentation.jpg"
        break;
      }
      case "application/x-rar":{
        this.imageSrc = "assets/images/rar.jpg"
        break;
      }
      case "application/rar":{
        this.imageSrc = "assets/images/rar.jpg"
        break;
      }
      case "application/vnd.google-apps.shortcut":{
        this.imageSrc = "assets/images/shortcut.jpg"
        break;
      }
      case "application/pdf":{
        this.imageSrc = "assets/images/pdf.jpg"
        break;
      }
      case "application/vnd.jgraph.mxfile":{
        this.imageSrc = "assets/images/Image.jpg"
        break;
      }
      case "application/msword":{
        this.imageSrc = "assets/images/officedocument.jpg";
        break;
      }
      default:{
        break;
      }
    }
  }

}
